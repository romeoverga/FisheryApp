package fishgame.fs.business;

import java.io.*;
import java.net.*;

public class Server {
    public static void main(String[] args) throws Exception
    {
        Server SERVER = new Server();
        SERVER.run();
    }
   
    public void run() throws Exception
    {
    	System.out.println("SERVER RUNNING.");
        ServerSocket SRVSOCK = new ServerSocket(444);
        Socket SOCK = SRVSOCK.accept();
        InputStreamReader IR = new InputStreamReader(SOCK.getInputStream());
        BufferedReader BR = new BufferedReader(IR);
       
        String MESSAGE = BR.readLine();
        System.out.println(MESSAGE);
       
        if (MESSAGE !=null)
        {
            PrintStream PS = new PrintStream(SOCK.getOutputStream());
            PS.println("Message recieved");
        }
        SRVSOCK.close();
    }
}
