package fishgame.fs.business;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ServerSocketManager {

		public static ArrayList<Socket> SocketArray = new ArrayList<Socket>();
		public static ArrayList<String> CurrentUsers = new ArrayList<String>();
		
		public static void main(String[]args) throws IOException
		{
			try
			{
				final int PORT = 444;
				ServerSocket SERVER = new ServerSocket(PORT);
				System.out.println("Waiting for clients...");
				
				while(true)	//infinite until break from thread X
				{
					Socket SOCK = SERVER.accept();
					SocketArray.add(SOCK);
					
					System.out.println("Client connected from:" + SOCK.getLocalAddress().getHostName());
		
					addUsername(SOCK);
					
					ServerSocket_Return GAMEROOM = new ServerSocket_Return(SOCK);
					Thread X = new Thread(GAMEROOM);
					X.start();
					
				}
			}
			catch(Exception X)	{	System.out.print(X);	}
			
		}
		
		public static void addUsername(Socket X) throws IOException
		{
			Scanner INPUT = new Scanner(X.getInputStream());
			String Username = INPUT.nextLine();
			CurrentUsers.add(Username);
			
			for(int i = 1; i <= ServerSocketManager.SocketArray.size(); i++)
			{
				Socket TEMP_SOCK = ServerSocketManager.SocketArray.get(i-1);
				PrintWriter OUT = new PrintWriter(TEMP_SOCK.getOutputStream());
				OUT.println("#?!" + CurrentUsers); 	//cmd characters
				OUT.flush();
			}
			INPUT.close();
		}
}
