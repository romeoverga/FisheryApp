package fishgame.fs.business;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class ServerSocket_Return implements Runnable {
	
	//globals
	Socket SOCK;
	private Scanner INPUT;
	private PrintWriter OUT;
	String MESSAGE = "";
	
	ServerSocket_Return(Socket X)
	{
		this.SOCK = X;
	}
	
	public void checkConnection() throws IOException
	{
		if(!SOCK.isConnected())
		{
			for(int i = 1; i <= ServerSocketManager.SocketArray.size(); i++)
				if(ServerSocketManager.SocketArray.get(i) == SOCK)
					ServerSocketManager.SocketArray.remove(i);
		
			for(int i = 1; i <= ServerSocketManager.SocketArray.size(); i++)	
			{
				Socket TEMP_SOCK = ServerSocketManager.SocketArray.get(i-1);
				PrintWriter TEMP_OUT = new PrintWriter(TEMP_SOCK.getOutputStream());
				TEMP_OUT.println(TEMP_SOCK.getLocalAddress().getHostName() + " disconnected!");
				TEMP_OUT.flush();
				//show disconnection at Server
				System.out.println(TEMP_SOCK.getLocalAddress().getHostName() + " disconnected!");
				
			}
		}
	}
	
	public void run()
	{
		try
		{
			try
			{
				INPUT = new Scanner(SOCK.getInputStream());
				//OUT = new PrintWriter(SOCK.getOutputStream());
				
				while(true)
				{
					checkConnection();
					
					if(!INPUT.hasNext()) {return;}
					
					MESSAGE = INPUT.nextLine();
					
					System.out.println("Client said: " + MESSAGE);
					
					for(int i = 1; i <= ServerSocketManager.SocketArray.size(); i++)
					{
						//Note: if necessary take CAST below out.I added to make it compile
						Socket TEMP_SOCK = ServerSocketManager.SocketArray.get(i-1);
						PrintWriter TEMP_OUT = new PrintWriter(TEMP_SOCK.getOutputStream());
						TEMP_OUT.println(MESSAGE);
						TEMP_OUT.flush();
						System.out.println("Sent to: " + TEMP_SOCK.getLocalAddress().getHostName());
					}//close for
				}//close while
			}//close inner try
			finally
			{
				SOCK.close();
			}
		}
		catch(Exception e){ System.out.print(e);}
		
	}
}
