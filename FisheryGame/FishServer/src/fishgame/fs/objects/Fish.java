package fishgame.fs.objects;
public class Fish {
	
	final static int MIN_HEALTH  = 10;
	final static int MAX_HEALTH  = 6000;
	final static double MIN_STAMINA = 0;
	final static double MAX_STAMINA = 100;
	
	private int health;		
	private double stamina;
	private double latPos;
	private double longPos;

	public Fish(int health, double stamina, double latPos, double longPos) {
		this.health = health;
		this.stamina = stamina;
		this.latPos = latPos;
		this.longPos = longPos;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public double getStamina() {
		return stamina;
	}

	public void setStamina(double stamina) {
		this.stamina = stamina;
	}

	public double getLatPos() {
		return latPos;
	}

	public void setLatPos(double latPos) {
		this.latPos = latPos;
	}

	public double getLongPos() {
		return longPos;
	}

	public void setLongPos(double longPos) {
		this.longPos = longPos;
	}

	public void incHealth(int value) // COMPLETE
	{
		if ((health + value) > MAX_HEALTH)
			health = MAX_HEALTH;
		else
			health += value;
	}

	public void incStamina(int value) // COMPLETE
	{
		if ((stamina + value) > MAX_STAMINA)
			stamina = MAX_STAMINA;
		else
			stamina += value;
	}

	public boolean useStamina() // COMPLETE
	{
		boolean out = stamina > 0;
		if (out)
			stamina--;
		return out;
	}
}
