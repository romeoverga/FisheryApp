package fishgame.fs.objects;

public class Food {

	private int value;
	private double latPos;
	private double longPos;

	public Food(int value, double latPos, double longPos) {
		this.value = value;
		this.latPos = latPos;
		this.longPos = longPos;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public double getLatPos() {
		return latPos;
	}

	public void setLatPos(double latPos) {
		this.latPos = latPos;
	}

	public double getLongPos() {
		return longPos;
	}

	public void setLongPos(double longPos) {
		this.longPos = longPos;
	}
}
