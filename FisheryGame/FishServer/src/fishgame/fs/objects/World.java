package fishgame.fs.objects;
import java.util.ArrayList;
import java.util.Random;

public class World {
	
	private static final int MAX_FOOD_VALUE = 3;
	private final int WORLD_RADIUS = 100;
	private final int MAX_FISH = 100;
	private final int MAX_FOOD = 12000;
	private final double STARTING_FOOD_SPACE = 2;
	private Random rand = new Random();
	
	private ArrayList<Fish> fishes;
	private ArrayList<Food> foods;
	
	public World() {
		this.fishes = new ArrayList<Fish>();
		this.foods = new ArrayList<Food>();
		generateWorld();
	}

	private void generateWorld() {
		double area = Math.PI * WORLD_RADIUS * WORLD_RADIUS;
		
		int numFood = (int) (area / STARTING_FOOD_SPACE);
		for (int i = 0; i < numFood; i++) {
			int value = rand.nextInt(MAX_FOOD_VALUE) + 1;
			double latPos = rand.nextDouble() * 360;
			double longPos = rand.nextDouble() * 360;
			
			Food food = new Food(value, latPos, longPos);
			foods.add(food);
		}
	}
	
	private boolean addFish(Fish fish) {
		if(fishes.size() < MAX_FISH) {
			fishes.add(fish);
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Fish> getFishes() {
		return fishes;
	}

	public void setFishes(ArrayList<Fish> fishes) {
		this.fishes = fishes;
	}

	public ArrayList<Food> getFoods() {
		return foods;
	}

	public void setFoods(ArrayList<Food> foods) {
		this.foods = foods;
	}
}